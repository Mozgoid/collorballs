﻿using UnityEngine;
using System.Collections;

public class BallGenerator : MonoBehaviour
{
    public GameObject prefab;

    public bool autoGenerate;
    public float generationTime;
    public float generationAreaRadius = 5;

    private float _timeToGenerate;

    void Update()
    {
        if (!autoGenerate) 
            return;

        _timeToGenerate -= Time.deltaTime;
        if (_timeToGenerate < 0)
        {
            Generate();
            _timeToGenerate = generationTime;
        }
    }


    public void Generate()
    {
        var deltaPos = Quaternion.AngleAxis(Random.Range(0, 360), Vector3.up)
            * Vector3.forward * Random.Range(0, generationAreaRadius);

        var b = Instantiate(prefab, transform.position + deltaPos, new Quaternion()) as GameObject;
        b.transform.parent = BallsManager.Instance.balls.transform;
    }


    private static BallGenerator[] _generators;
    public static void StaticGenerate()
    {
        if (_generators == null)
        {
            _generators = FindObjectsOfType<BallGenerator>();
        }

        foreach (var ballGenerator in _generators)
        {
            ballGenerator.Generate();
        }
    }

}
