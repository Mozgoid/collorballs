﻿using System;
using UnityEngine;
using System.Collections;

[System.Serializable]
public class BallProperties
{
    public Color color;
    public float radius = 1;
    public int points = 10;

    public BallProperties Clone()
    {
        return new BallProperties
        {
            color = color,
            radius = radius,
            points = points,
        };
    }
}
