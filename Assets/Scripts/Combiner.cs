﻿using UnityEngine;
using System.Collections;




public class Combiner : SingletonMonoBehaviour<Combiner> 
{
    private ICombineRule _rule;

    public ICombineRule Rule
    {
        get
        {
            if (_rule == null)
            {
                _rule = GetComponentInChildren(typeof(ICombineRule)) as ICombineRule;
            }
            return _rule;
        }
    }


    public bool TryCombineBalls(Ball a, Ball b)
    {
        if (a == b || !a.isActiveAndEnabled || !b.isActiveAndEnabled)
            return false;

        return Rule.TryCombineBalls(a, b);
    }


    public void MakeOneBallFromTwo(Ball a, Ball b)
    {
        //var pos = (a.transform.position + b.transform.position) / 2;


        Destroy(b.GetComponent<Ball>());
        Destroy(b.GetComponent<Rigidbody>());
        Destroy(b.GetComponent<Collider>());
        Destroy(b.GetComponent<BallDrag>());
        b.gameObject.AddComponent<MergeWithOther>().Init(a);

        //a.transform.position = pos;
        a.GetComponent<Rigidbody>().velocity = Vector3.zero;
    }



}
