﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;


public class CombineSameCollors : MonoBehaviour, ICombineRule
{
    public float radiusCoefficient = 1;
    public BallProperties[] ballVariants;

    private float maxBall;

    bool ICombineRule.TryCombineBalls(Ball a, Ball b)
    {
        var comb = Combiner.Instance;

        if (a.Propirties.color != b.Propirties.color)
            return false;

        var biggerBall = a.Propirties.points > b.Propirties.points ? a : b;
        var smallerBall = biggerBall == a ? b : a;

        biggerBall.Propirties.points += smallerBall.Propirties.points;
        biggerBall.Propirties.radius = (float)Math.Sqrt(biggerBall.Propirties.points / Math.PI) * radiusCoefficient;
        biggerBall.Propirties = biggerBall.Propirties;

        comb.MakeOneBallFromTwo(biggerBall, smallerBall);

        if (maxBall < biggerBall.Propirties.points)
        {
            maxBall = biggerBall.Propirties.points;
            Debug.Log(maxBall);
        }

        return true;
    }

    public void InitBall(Ball ball)
    {
        var prop = ballVariants[Random.Range(0, ballVariants.Length)].Clone();
        prop.radius = (float)Math.Sqrt(prop.points / Math.PI) * radiusCoefficient;
        ball.Propirties = prop;
    }
}
