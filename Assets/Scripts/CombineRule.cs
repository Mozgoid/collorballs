﻿using UnityEngine;
using System.Collections;

public interface ICombineRule 
{
    bool TryCombineBalls(Ball a, Ball b);
    void InitBall(Ball ball);
}
