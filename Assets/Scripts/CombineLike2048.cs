﻿using UnityEngine;
using System.Collections;

public class CombineLike2048 : MonoBehaviour, ICombineRule 
{
    public BallProperties[] ballLevels;

    bool ICombineRule.TryCombineBalls(Ball a, Ball b)
    {
        var comb = Combiner.Instance;

        if (a.Level != b.Level || a.Level >= (ballLevels.Length - 1))
            return false;

        a.Level++;
        a.Propirties = ballLevels[a.Level];
        comb.MakeOneBallFromTwo(a, b);
        return true;
    }

    public void InitBall(Ball ball)
    {
        ball.Propirties = ballLevels[ball.Level];
    }
}
