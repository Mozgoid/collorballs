﻿using UnityEngine;
using System.Collections;

public class BallsManager : SingletonMonoBehaviour<BallsManager>
{

    public GameObject ballsPrefab;
    public GameObject balls;

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.F1))
        {
            LoadBalls();
        }
    }

    void Start()
    {
        LoadBalls();
    }

    void LoadBalls()
    {
        if (balls != null)
        {
            Destroy(balls);
        }
        balls = Instantiate(ballsPrefab);
    }



}
