﻿using UnityEngine;
using System.Collections;

public class MergeWithOther : MonoBehaviour
{
    public float speed = 2;
    private Transform _target;
    private Vector3 _delta;
    private Vector3 _normDelta;

    public void Init(Ball other)
    {
        _delta = transform.position - other.transform.position;
        _normDelta = _delta.normalized;
        _target = other.transform;
    }

    void Update()
    {
        var distanceOnFrame = speed*Time.deltaTime;
        if (_delta.magnitude < distanceOnFrame)
        {
            Destroy(gameObject);
        }
        else
        {
            _delta -= _normDelta*distanceOnFrame;
            transform.position = _target.position + _delta;
        }
    }
}
