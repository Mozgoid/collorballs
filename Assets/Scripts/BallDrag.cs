﻿using System;
using UnityEngine;
using System.Collections;

public class BallDrag : MonoBehaviour
{
    private Vector3 _startMousePos;
    private Rigidbody _rigidbody;
    private float _timeToCancel;


    [Serializable]
    public class DragParams
    {
        public float minForce = 300;
        public float maxForce = 1000;

        [Tooltip("если игрок потянет шар так быстро то он полетит с максимальной скоростью")]
        public float fastestDragTime = 0.2f;
        [Tooltip("если игрок тянет шар больше этого времени то он полетит с минимальной скоростью")]
        public float slowestDragTime = 3;
    }

    public DragParams dragParams;


    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    void OnMouseDown()
    {
        _startMousePos = Input.mousePosition;
        _timeToCancel = dragParams.slowestDragTime;
    }

    void Update()
    {
        _timeToCancel -= Time.deltaTime;
    }

    void OnMouseUp()
    {
        if(_timeToCancel < 0)
            return;

        var dragTime = dragParams.slowestDragTime - _timeToCancel;
        var dragDirection = (Input.mousePosition - _startMousePos).normalized;
        var force = dragTime < dragParams.fastestDragTime
            ? dragParams.maxForce
            : dragParams.minForce + (_timeToCancel / dragParams.slowestDragTime) * (dragParams.maxForce - dragParams.minForce);

        Debug.LogFormat("result force {0}", force);
        var dragVector = dragDirection*force;
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.AddForce(dragVector.x, 0, dragVector.y);

        BallGenerator.StaticGenerate();
    }
}
