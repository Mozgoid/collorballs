﻿using System;
using UnityEngine;
using System.Collections;




public class Ball : MonoBehaviour 
{
    private BallProperties _properties;
    private Renderer _renderer;

    public float growSpeed = 1;

    public int Level { get; set; }

    public BallProperties Propirties
    {
        set 
        {
            _properties = value;
            _renderer.material.color = _properties.color;
        }
        get { return _properties; }
    }

    void Start()
    {
        var r = 0.1f;
        transform.localScale = new Vector3(r, r, r);
        _renderer = GetComponent<Renderer>();
        Combiner.Instance.Rule.InitBall(this);
    }


    void Update()
    {
        Grow();
    }


    void Grow()
    {
        var currentR = transform.localScale.x;
        var diff = currentR - Propirties.radius;
        var absDiff = Math.Abs(diff);

        if (absDiff < 0.05)
            return;

        var frameGrow = Time.deltaTime * growSpeed;
        var newR = absDiff < frameGrow
            ? _properties.radius
            : currentR < Propirties.radius
                ? currentR + frameGrow
                : currentR - frameGrow;

        transform.localScale = new Vector3(newR, newR, newR);
        var pos = transform.localPosition;
        pos.y = newR / 2;
        transform.localPosition = pos;
    }


    void OnCollisionEnter(Collision other)
    {
        if (Propirties != null && other.gameObject.CompareTag("Ball"))
        {
            Combiner.Instance.TryCombineBalls(this, other.gameObject.GetComponent<Ball>());
        }
    }



}
